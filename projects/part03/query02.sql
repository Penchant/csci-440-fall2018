.read data.sql

--Delete from Likes
--       where ID2 in ( Select ID2 from Friend where Likes.ID1 =ID1) and ID2 not in ( Select likes2.ID1 from Likes likes2 where likes2.ID1 = likes2.ID2); 

/*Delete from Likes
       where ID1 in
       	     (Select A.ID1 from Friend A, Likes LA where A.ID1 = LA.ID1 and A.ID2 = LA.ID2 and A.ID2 not in (select ID1 from Likes where ID2 = A.ID1));
*/

DELETE FROM Likes
where exists(
SELECT *
FROM Friend B
WHERE (B.ID1 =Likes.ID1) and (B.ID2=Likes.ID2))
AND NOT EXISTS (SELECT * FROM Likes LB
WHERE (Likes.ID1=LB.ID2) and (Likes.ID2=LB.ID1));
