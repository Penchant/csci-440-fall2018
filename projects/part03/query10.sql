.read data.sql

SELECT A.name, A.grade, B.name, B.grade
FROM Highschooler A
INNER JOIN Likes on A.ID = Likes.ID1
INNER JOIN Highschooler B
ON B.ID = Likes.ID2
WHERE (A.grade - B.grade) >= 2;
