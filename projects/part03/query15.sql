.read data.sql

SELECT DISTINCT A.name, A.grade, B.name, B.grade, C.name, C.grade
FROM Highschooler A, Highschooler B, Highschooler C, Likes L, Friend F1, Friend F2
WHERE (A.ID = L.ID1 and B.ID = L.ID2) and B.ID not in ( SELECT ID2 from Friend
WHERE ID1 = A.ID and (A.ID = F1.ID1 and C.ID = F1.ID2) and (B.ID = F2.ID1 and C.ID = F2.ID2));
