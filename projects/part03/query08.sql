.read data.sql

SELECT name, grade
FROM Highschooler
INNER JOIN Friend on Highschooler.ID = Friend.ID1
GROUP BY ID1
HAVING COUNT(*) = (
       SELECT MAX(count)
       FROM (
       	    SELECT COUNT(*) as COUNT
	    FROM FRIEND
	    GROUP BY ID1));
