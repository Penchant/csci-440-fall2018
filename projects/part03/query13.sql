.read data.sql

SELECT A.name, A.grade, B.name, B.grade
FROM Highschooler A
INNER JOIN Likes on A.ID = Likes.ID1
INNER JOIN Highschooler B on B.ID = Likes.ID2
WHERE (A.ID = Likes.ID1 AND B.ID = Likes.ID2) and B.ID
NOT In (
SELECT DISTINCT ID1
FROM Likes);
