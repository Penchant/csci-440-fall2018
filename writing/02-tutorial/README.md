Prerequisites

- Must have sqlite 3 installed
  - First, go to the https://www.sqlite.org/download.html website.
  - Download the appropriate version.
  - Extract the zip file.
  - To verify the installation:
    - Open cmd and navigate to your sqlite directory
    - Type sqlite3 and press enter. This should show the version of sqlite3 that you have.
- SELECT statements
  - Introduction
    - SELECT (Columns) FROM (Table) WHERE (condition)
  - SELECT DISTINCT
  - CONDITIONS
    - AND, OR, NOT
    - IS NULL
    - WHERE (column) LIKE (pattern)
    - IN(value1, value2, …)|(SELECT Statement)
    - BETWEEN value1 AND value2
  - ORDER BY
    - ASC
    - DESC
  - SELECT TOP(number)
  - SELECT MIN(column)|MAX(column)
  - SELECT COUNT(column)
  - SELECT AVG(column)
  - SELECT SUM(column)
  - SELECT (column) AS (Alias\_Name) FROM (Table)
  - SELECT (table1.column) FROM (table1) INNER JOIN (table2) ON (column)
  - SELECT (column) FROM (table) GROUP BY (column)
  - HAVING
  - Exists

SQL Select Statements

**Introduction**

In SQL the SELECT statement is used to return a result from one or many tables within a given database. Within the database the SELECT statement is more commonly used as a data query command. This is where the query is used to retrieve information from zero or more columns, using one or many database tables.

In order to understand the method of using the SELECT statement properly the correct database management system must be installed. For this tutorial on the SELECT statement, sqlite version three must be properly installed. The best location for installing sqlite is:

https://www.sqlite.org/download.html

The operating system in use will determine which sqlite package zip file to download and install. Once the zip file has been downloaded, the user must first extract the files from the folder by right clicking the zipped folder icon and selecting the extract all option. A window will pop up asking for the location to extract the files and this will be up to the user on where they would like to keep the file located. The location for the file will need to be added to the path for the installation process to be completed.

To add the files to the path the user must go to the control panel, locate the &#39;environment&#39;. The user will then select edit the system and environment variables. A window will appear and the user will select environment variables. Under the system variables table, select the path variable. The user will then select edit under the path variable. A new window will appear, this is where the user must click new. This is where the folder location of the sqlite files are located.

**Statement Types**

**Select Distinct**

When running queries against the database, a common issue can be that multiple identical rows are returned when only unique entries are desired to be returned from the query. This is issue is resolved in SQL with the SELECT DISTINCT statement, which returns only distinct values. For example:

  SELECT DISTINCT Country FROM Customers

The above statement allows a query to be made to find all the unique countries that customers are from in easily viewable manner as manually scanning the results for unique entries when there could easily be thousands of results would be quite cumbersome.

**Select Statement Conditionals**

When using SELECT statements it can often be useful to only grab certain rows from the tables being queried depending on some condition. SQL allows for this using the WHERE modifier on SELECT statements such that it only selects rows where a particular condition is true.

 Example:

SELECT \* FROM Customers WHERE Country = &quot;USA&quot;

By running the above statement, customers from the country USA are returned. In general, rows can be conditionally selected with the following format:

SELECT {Columns} FROM {Tables} WHERE {Condition}

In addition to checking if a property matches a particular value, SELECT statements have a variety of conditional operators which include: AND, OR, NOT, IS NULL, LIKE, IN, and BETWEEN.





**Select Statement Conditionals: AND**

The AND operator in SQL allows logical statements to be modified such that in order for the statement to be evaluated as true, both conditions on either side of the AND operator must be true. This is important for being able to require 2 differents conditions.

Example:

SELECT \* From Customers WHERE Country = &quot;USA&quot; AND Sales \&gt; 500

The AND operator allows the selection of customers from the USA with sales of at least 500 with that customer.

 General Format:

SELECT {Columns} FROM {Tables} WHERE {Condition1} AND {Condition2}

**Select Statement Conditionals: OR**

The OR operator in SQL allows logical statements to be modified such that in order for the statement to be evaluated as true, one or both conditions on either side of the OR operator must be true.

**Select Statement Conditionals: NOT**

The NOT operator in SQL functions as a logical negation.

 Example:

  SELECT \* FROM Customers WHERE (item1) NOT (item2)

**Select Statement Conditionals: IS NULL**

In SQL the IS NULL operator is used to test for NULL values. This operators main use is to find the NULL values in a specified column.

**Select Statement Conditionals: IN**

Using the IN operator allows for the easy checking of if a value is in a value set.

 Example:

SELECT \* FROM Customers Where Id IN

(SELECT Id From VIPCustomers WHERE Country = &quot;USA&quot;)

The above select statement allows the easy selection customers where they are a VIP Customer from the USA. The VIPCustomers table may only track a small information about the customer compared to the regular customers table, thus the benefit of grabbing these customers from the Customers table by referencing the VIPCustomers table.

 General Format:

SELECT {Columns} FROM {Tables} WHERE {Value} IN {Value Set}

**Select Statement Conditionals: LIKE**

The LIKE operator is useful for searching through large rows. It can be utilized with regular expressions to search by pattern through a table.

 Example:

  SELECT \* FROM Customers WHERE (column) LIKE (regular expression)

**Select Statement Conditionals: BETWEEN**

To find information within a specific range the BETWEEN operator is used. The values can be text, numbers, or dates.



**ORDER BY**

ORDER BY is a keyword that is used to sort data in ascending a descending order. The ascending order is always the default sorting technique used by ORDER BY. when the descending order in needed the specifier of DESC must be implemented at the end of the query. However, the method to use ascending and then descending order is described by placing ASC|DEAC at the end of the query. Where ASC is the signifier for ascending order.

**SELECT TOP(number)**

Often times it is desirable to only select the beginning of a list, which can be done in SQL with the SELECT TOP statement that returns up to the number passed.

 Example:

SELECT TOP 10 FROM Customers ORDER BY Sales

 Selecting the top 10 customers by sales is able to be done with the above statement. In general the SELECT TOP statement is used as follows:

SELECT TOP {Number} FROM {Tables}

**SELECT MIN(column)|MAX(column)**

MIN() returns the smallest value in the selected column

 Example:

   SELECT MIN({columnName}) FROM {tableName} WHERE {condition};

MAX() returns the largest value in the selected column:

 Example:

   SELECT MAX({columnName}) FROM {tableName} WHERE {condition};

**SELECT COUNT(column)**

When looking at data it can often be helpful to know how many occurences there are of something, which is completed through using the SELECT COUNT statement.

 Example:

SELECT COUNT(\*) FROM Customers WHERE Country = &quot;USA&quot;

This statement allows the number of customers from the USA to be returned.

 General Format:

SELECT COUNT({Columns}) FROM {Tables}

**SELECT AVG(column)**

Using a numeric based column only, the average can be achieved using the syntax of AVG().

 Example:

  SELECT ({columnName}) FROM {tableName} WHERE {condition};

**SELECT SUM(column)**

The SUM keyword allows the summation of the values in a column.

 Example:

SELECT SUM(Sales) FROM Customers WHERE Country = &quot;USA&quot;

 This statement returns the total sales from all US Customers. The general format is as follows:

SELECT SUM({Column}) FROM {Tables}

**SELECT (column) AS (Alias\_Name) FROM (Table)**

Sometimes a column or table in SQL need a temporary name change. This can be performed through aliasing.

 Example:

   SELECT {columnName} AS {aliasName} FROM {tableName} WHERE;

**SELECT (table1.column) FROM (table1) INNER JOIN (table2) ON (column)**

In SQL  the INNER JOIN keywords selects data from different tables with matching values.

 Example:

SELECT {columnName} FROM {FirstTable} INNER JOIN {SecondTable} ON {FirstTable.columnName} = {SecondTable.columnName};

**SELECT (column) FROM (table) GROUP BY (column)**

When working data it can at times be useful to group the results by a particular column.

 Example:

SELECT SUM(Sales), Country FROM Customers GROUP BY Country.

The above statement allows the total sales for each country customers are in to be returned.

 General Format:

SELECT {Columns} FROM {Tables} GROUP BY {Column}

**HAVING**

At times, conditionals on grouped data can be desirable, that is using conditional logic after using the GROUP BY keyword which can be accomplished using the HAVING keyword.

 Example:

SELECT Country FROM Customers GROUP BY Country HAVING SUM(Sales) \&gt; 1,000,000

The above statement allows the selection of countries where the total sales from customers in that country is over 1,000,000.

 General Format:

SELECT {Columns} FROM {Tables} GROUP BY {Column} HAVING {Condition}

**Exists**

Often times knowing whether a value exists in a value set can be quite useful and is accomplished with the EXISTS keyword.

 Example:

SELECT {Columns} FROM {Tables} WHERE EXISTS (

SELECT {Columns} FROM {Tables} WHERE {Condition})

